import httpcodes
import json
import os
from user_directory import store_new_user, find_user, get_user_password, list_users
from classes import Band, User, load_band
from flask import Flask, request, render_template, redirect, g,\
send_from_directory, session, flash, url_for

app = Flask(__name__)

################# HOME ########################
@app.route('/', methods=['GET'])
def welcome_page():
    loggedin = session.get('logged in', False)
    if not loggedin:
        return render_template('login.html')
    else:
        return app.send_static_file('index.html'), httpcodes.OK

############ ACCOUNT MANAGEMENT ############################
@app.route('/login', methods=['POST'])
def login():
    username = request.form['username'].strip()
    returned_password = request.form['password'].strip()
    if find_user(username):
        if get_user_password(username, returned_password):
            session['logged in'] = True
            session['user'] = username
    else:
        flash('Wrong credentials')
    return redirect(url_for('welcome_page'))

@app.route("/logout")
def logout():
    session.pop('logged in', None)
    session.pop('user', None)
    return welcome_page()

@app.route('/account', methods=['GET', 'POST'])
def create_account():
    if request.method == 'GET':
        return render_template('createaccount.html')
    elif request.method == 'POST':
        if request.form['password'] != request.form['repeat-password']:
            flash('Passwords do not match')
            return redirect(url_for('create_account'))
        else:
            if find_user(request.form['username'].strip()):
                flash('Username taken')
                return redirect(url_for('create_account'))
            newUser = User(request.form['username'].strip(),
                    request.form['password'].strip(),
                    request.form.get('email', None))
            store_new_user(newUser)            
            return welcome_page()

############# NEW BAND ##########################
@app.route('/new', methods=['GET','POST'])
def new_band():
    if session.get('logged in'):
        newBand = Band(None) 
        newBand.user = session['user']
        if request.method == 'GET':
            return render_template('new_blankband.html', band = newBand), httpcodes.OK
        elif request.method == 'POST':
            name = request.form['bandname'] 
            newBand.name = None if not name else name
            if 'ispublic' in request.form.keys():
                newBand.public = True
            newBand.captain['Specialism'] = request.form['capspec']
            #Allows for multiple skills to be selected
            newBand.parseSkills(request.form['capskill'], True)
            #Only allows a single weapon per captain and per ensign
            newBand.captain['Items'] = [request.form['capweap']]
            if 'hasensign' in request.form.keys():
                newBand.hasEnsign = True
                newBand.ensign['Specialism'] = request.form['ensspec']
                newBand.parseSkills(request.form['ensskill'], False)
                newBand.ensign['Items'] = [request.form['ensweap']]
                newBand.size += 1
            
            troops = json.loads(request.form['troops'])
            for item in troops:
                if item != "Empty":
                    newBand.members.append(item)
                    newBand.size += 1
            
            newBand.newBandMoney()
            
            if not newBand.validate():
                return render_template('new_blankband.html', band = newBand),
                httpcodes.BAD_REQUEST
            else:
                if newBand.store(True):
                    #On successful creation of a band the user is returned to the homescreen
                    return welcome_page()
                else:
                    return render_template('new_blankband.html', band = newBand),
                    httpcodes.BAD_REQUEST


    else:
        return welcome_page() 


############ EDIT EXISTING #####################
@app.route('/edit', methods=['GET'])
def select_page():
    if session.get('logged in'):
        bandDir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands") 
        if os.path.isdir(bandDir):
            userDir = os.path.join(bandDir, session['user'])
            publicDir = os.path.join(bandDir, 'public')
            if os.path.isdir(userDir):
                myBands = os.listdir(userDir)
            else: 
                os.mkdir(userDir)
                myBands = None
            if os.path.isdir(publicDir):
                publicBands = os.listdir(publicDir)
            else:
                os.mkdir(publicDir)
                publicBands = None
        else:
           os.mkdir(bandDir)
           myBands = None
           publicBands = None
        return render_template('bandlist.html', my_bands=myBands,
                public_bands=publicBands), httpcodes.OK
    else:
        return welcome_page()

@app.route('/edit/<bandname>', methods=['GET','POST'])
def edit_band(bandname):
    if session.get('logged in'):
        username = session['user']
        band = load_band(bandname,username, False)
        if band == False:
            return redirect(url_for('select_page'))
        if request.method == 'GET':
            return render_template('editband.html', band = band), httpcodes.OK
        elif request.method == 'POST':
            #name = request.form['bandname'] 
            #band.name = None if not name else name
            band.public = True if request.form['ispublic']=='true' else False
            band.size = 1
            band.captain['Specialism'] = request.form['capspec']
            #Allows for multiple skills to be selected
            band.parseSkills(request.form['capskill'], True)
            #Only allows a single weapon per captain and per ensign
            band.captain['Items'] = [request.form['capweap']]
            band.captain['Move'] = request.form['capmove']
            band.captain['Fight'] = request.form['capfight']
            band.captain['Shoot'] = request.form['capshoot']
            band.captain['Shield'] = request.form['capshield']
            band.captain['Morale'] = request.form['capmorale']
            band.captain['Health'] = request.form['caphealth']
            band.captain['Experience'] = request.form['capexperience']
            band.hasEnsign = True if request.form['hasensign'] == 'true' else False
            if band.hasEnsign:
                band.ensign['Specialism'] = request.form['ensspec']
                band.parseSkills(request.form['ensskill'], False)
                band.ensign['Items'] = [request.form['ensweap']]
                band.size += 1
                band.ensign['Move'] = request.form['ensmove']
                band.ensign['Fight'] = request.form['ensfight']
                band.ensign['Shoot'] = request.form['ensshoot']
                band.ensign['Shield'] = request.form['ensshield']
                band.ensign['Morale'] = request.form['ensmorale']
                band.ensign['Health'] = request.form['enshealth']
                band.ensign['Experience'] = request.form['ensexperience']
            troops = json.loads(request.form['troops'])
            previous_members = band.members
            band.members = []
            for item in troops:
                if item != "Empty":
                    band.members.append(item)
                    band.size += 1

            band.editBandMoney(previous_members)

            if not band.validate():
                return render_template('editband.html', band = band),
                httpcodes.BAD_REQUEST
            else:
                if band.store(False):
                    #On successful creation of a band the user is returned to the homescreen
                    return redirect(url_for('select_page'))
                else:
                    return render_template('editband.html', band = band),
                    httpcodes.BAD_REQUEST
    else:
        return welcome_page()

######## VIEW EXISTING ########################

@app.route('/view/public/<bandname>', methods=['GET'])
def view_band(bandname):
    band = load_band(bandname, None, True)
    if band == False:
        return redirect(url_for('select_page'))
    if request.method == 'GET':
        return render_template('viewband.html', band=band)

######### DELETE ################################

@app.route('/delete/<bandname>', methods=['GET'])
def delete_given(bandname):
    user = session.get('user')
    band = load_band(bandname, user, False)
    if band:
        band.remove()
    return redirect(url_for('select_page'))

############## DEBUG ############################
#@app.route('/check_users')
#def check_users():
#    list_users()
#    return welcome_page()
#
#@app.route('/find_users/<username>')
#def find_users(username):
#    print find_user(username)
#    return welcome_page()
###############################################

if __name__=="__main__":
    app.secret_key = os.urandom(12)
    app.run(host='0.0.0.0', port=5000)

