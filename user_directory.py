import json
import os
from werkzeug.security import check_password_hash

#Adds new user to json user file
def store_new_user(user):
    if os.path.isfile(os.path.join(os.path.dirname(os.path.realpath(__file__)),
        'users')):
        new_entry = {'username': user.username, 'password': user.password,
                'email': user.email}
        with open('users', 'r') as read_file:
            orig = json.load(read_file)
        with open('users', 'w') as write_file:
            orig.append(new_entry)
            json.dump(orig, write_file)
    else:
        with  open('users', 'w') as f:
            json.dump([],f)
        store_new_user(user)

#Checks to see if user has already been added 
def find_user(username):
    if os.path.isfile(os.path.join(os.path.dirname(os.path.realpath(__file__)),
        'users')):
        with open('users', 'r') as f:
            users = json.load(f)
        for item in users:
            if username == item['username']:
                return True
        return False
    else:
        return False

#Checks to see if password given matches stored password
def get_user_password(username, given_password):
    if os.path.isfile(os.path.join(os.path.dirname(os.path.realpath(__file__)),
        'users')):
        with open('users', 'r') as f:
            users = json.load(f)
        for item in users:
            if username in item['username']:
                return check_password_hash(item['password'], given_password)
    return False

#Remove user by username
def remove_user(username):
    if os.path.isfile(os.path.join(os.path.dirname(os.path.realpath(__file__)),
        'users')):
        with open('users', 'r') as f:
            users = json.load(f)
        for item in users:
            if username == item['username']:
                users.remove(item)
                with  open('users', 'w') as f:
                    json.dump(users,f)
                return True
    return False

#debug only
def list_users():
    if os.path.isfile(os.path.join(os.path.dirname(os.path.realpath(__file__)),
        'users')):
        f = open('users', 'r')
        users = json.load(f)
        print users
    else:
        print "No users"

