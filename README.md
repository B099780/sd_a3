
# Setup
Ensure the necessary python modules are installed.
The necessary python modules are:

* flask
* Flask-Testing

There needs to be a directory called 'bands' in the same directory as the framework.py file in order for the storage of bands to work correctly.

This web app was built and tested on MacOS.

# Running the app
To run the app locally run the script start_app.sh


	> ./start_app.sh

The app can now be accessed from a web browser at the address

	0.0.0.0:5000
	

	





