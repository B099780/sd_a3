import os
import pickle
from werkzeug.security import generate_password_hash, check_password_hash

class Band(object):
    specialisms = [ 'Engineering', 'Psychology', 'Marksman', 'Tactics', 'Melee', 'Defence' ]
    skillsets = { 'Engineering' : ['Repair', 'Sabotage', 'Augment'], 
            'Psychology': [ 'Bolster', 'Terror', 'Counter'], 
            'Marksman':['Aim', 'Pierce', 'Reload'], 
            'Tactics': ['Squad', 'Ambush', 'Surround'], 
            'Melee': ['Block', 'Risposte', 'Dual'], 
            'Defence': ['Shield', 'Sacrifice', 'Resolute'] }

    weapon = [ 'Blaster', 'Needle Gun', 'Blade', 'Cannon', 'Whip']
    cost = { 'Blaster':5, 'Needle Gun':12, 'Blade':3, 'Cannon':15, 'Whip':5}

    memberDefs = {'Augment Gorilla': 
            {'Move':8, 'Fight':3, 'Shoot':0, 'Shield':10, 'Morale':2, 
                'Health':8, 'Cost':20, 'Notes':'Animal, Cannot carry treasure or items'}, 
            'Lackey': 
            {'Move':6, 'Fight':2, 'Shoot':0, 'Shield':10, 'Morale':-1, 
                'Health':10, 'Cost':20,'Notes':'Melee Weapon'},  
            'Security': 
            {'Move':6, 'Fight':2, 'Shoot':1, 'Shield':12, 'Morale':2, 
                'Health':12, 'Cost':80, 'Notes':'Blaster, Blade'}, 
            'Engineer': 
            {'Move':4, 'Fight':0,'Shoot':3, 'Shield':12, 'Morale':2, 
                'Health':10, 'Cost':60, 'Notes':'Blaster, Repair Kit'}, 
            'Medic': 
            {'Move':5, 'Fight':0, 'Shoot':0, 'Shield':12, 'Morale':3,
                'Health':10, 'Cost':50, 'Notes':'Blade, Medkit'},
            'Commando': 
            {'Move':8, 'Fight':4, 'Shoot':0, 'Shield':10,'Morale':4, 
                'Health':12, 'Cost':100, 'Notes':'Stealth Suit, Blade, Needle Gun'}, 
            'Combat Droid': 
            {'Move':3, 'Fight':2, 'Shoot':4, 'Shield':14, 'Morale':0,
                'Health':14, 'Cost':150, 'Notes':'Mechanoid, Dual Blaster, Claws'}}


    def __init__(self, name):
        self.name = name
        self.captain = {'Move': 5, 'Fight': 2, 'Shoot': 2, 'Shield': 12, 'Morale': 4, 
                'Health': 12, 'Cost': 0, 'Skillset':[], 'Specialism': None, 'Items': [], 'Experience':0}
        self.ensign = {'Move':7, 'Fight':0, 'Shoot':-1, 'Shield':10, 'Morale':2, 'Health':8,
                'Skillset': [], 'Specialism': None, 'Cost':250, 'Items':[], 'Experience':0}
        self.hasEnsign = False

        self.members = []
        self.size = 1
        self.money = 500
        self.public = False
        self.user = None

    ''' 
    Function to parse the string of selected skills returned by the form on
    the webpage.
    '''
    def parseSkills(self, skillstring, captain):
        skilllist = skillstring.split(",")
        if captain :
            self.captain['Skillset'] = skilllist
        else:
            self.ensign['Skillset'] = skilllist
    '''
    Function to deduct the correct amount of money for a newly created band
    '''
    def newBandMoney(self):
        total = 0
        for item in self.captain['Items']:
            total += self.cost[item]
        if self.hasEnsign:
            total += 250
            for item in self.ensign['Items']:
                total += self.cost[item]
        for member  in self.members:
            total += self.memberDefs[member]['Cost']
        self.money -= total
        return True
    '''
    Function to calculate the correct amount of money after a band is edited.
    Currently incomplete - only calculates deductions based on any new members
    added. Doesn't take into account the addition of an ensign or any items.
    '''
    def editBandMoney(self, prev):
        for member in self.members:
            if member not in prev:
                self.money -= self.memberDefs[member]['Cost']
            else: 
                prev.remove(member)
        return True

    '''
    Validates band by basic criteria.
    Incomplete - doesn't take into account relation between experience and skill
    points etc.
    '''
    def validate(self):
        if self.size > 10:
            return False
        if self.name == None or self.user == None:
            return False
        if self.money < 0:
            return False
        return True

  
    '''
    Creates file with the same name as band and stores serialized version of the
    band in it. 
    If the band is a public band stores a copy in the public folder
    as well as the user's folder.
    If the band is not public and a public saved version exists removes that
    version.
    If create flag is set to True returns False if the file already
    exists.
    '''
    def store(self, create):
        userDir = os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),"bands"), self.user)
        filename = os.path.join(userDir, self.name)
        filenamepublic = os.path.join(os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),"bands"),"public"), self.name)
        if not os.path.exists(userDir):
            os.mkdir(userDir)
        if self.public:
            if create and os.path.isfile(filenamepublic):
                return False
            with open(filenamepublic, "wb") as f:
                pickle.dump(self, f)
        else:
            if os.path.isfile(filenamepublic):
                os.remove(filenamepublic)
        if create and os.path.isfile(filename):
            return False
        with open(filename, "wb") as f:
            pickle.dump(self, f)
        return True
    
    '''
    Deletes the stored file of the band and also the public version if the band
    is public.
    '''
    def remove(self):
        userDir = os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),"bands"), self.user)
        filename = os.path.join(userDir, self.name)
        if not os.path.isfile(filename):
            return False
        filenamepublic = os.path.join(os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),"bands"),"public"), self.name)
        if self.public:
            if os.path.isfile(filenamepublic):
                os.remove(filenamepublic)
        os.remove(filename)
        return True
''' 
Function which returns the band stored as the given bandname if it exists.
The public variable toggles whether it looks for the band in the public folder
or in the user's folder.
It returns False if it cannot find the file.
'''
def load_band(bandname, username, public):
    if public:
        filename = os.path.join(os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),"bands"),
            "public"), bandname)
        if os.path.isfile(filename): 
            with open(filename, "rb") as f:
                return pickle.load(f)
    else:
        filename = os.path.join(os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),"bands"),username), bandname)
        if os.path.isfile(filename): 
            with open(filename, "rb") as f:
                return pickle.load(f)
    
    return False

class User(object):
    def __init__(self, username, password, email):
        self.username = username
        self.password = self.hash_password(password)
        if email is not None:
            self.email = email
    def hash_password(self, password):
        return  generate_password_hash(password)



