var total = {{band.money}};
var engHire = false;

function hireEng()
{
  if(engHire == false)
  {
    engHire = true;
    document.getElementById("ensignDiv").style.display = "block";
    total = total - 250;
    updateCost();
  }
  else
  {
    engHire = false;
    document.getElementById("ensignDiv").style.display = "none";
    total = total + 250;
    updateCost();
  }
}

function updateTableRow(row, x)
{
  var sel = x.value;

  var myTable = document.getElementById('rosterTable');

  if(sel == "Empty")
  {
    myTable.rows[row].cells[1].innerHTML = '-';
    myTable.rows[row].cells[2].innerHTML = '-';
    myTable.rows[row].cells[3].innerHTML = '-';
    myTable.rows[row].cells[4].innerHTML = '-';
    myTable.rows[row].cells[5].innerHTML = '-'
    myTable.rows[row].cells[6].innerHTML = '-';
    myTable.rows[row].cells[7].innerHTML = 0;
    myTable.rows[row].cells[8].innerHTML = '-';
  }
  {% for troop in band.memberDefs.keys() %}
  if(sel == "{{troop}}")
  {
    myTable.rows[row].cells[1].innerHTML = '{{ band.memberDefs[troop]['Move'] }}';
    myTable.rows[row].cells[2].innerHTML = '{{ band.memberDefs[troop]['Fight'] }}';
    myTable.rows[row].cells[3].innerHTML = '{{ band.memberDefs[troop]['Shoot'] }}';
    myTable.rows[row].cells[4].innerHTML = '{{ band.memberDefs[troop]['Shield'] }}';
    myTable.rows[row].cells[5].innerHTML = '{{ band.memberDefs[troop]['Morale'] }}';
    myTable.rows[row].cells[6].innerHTML = '{{ band.memberDefs[troop]['Health'] }}';
    myTable.rows[row].cells[7].innerHTML = {{ band.memberDefs[troop]['Cost'] }};
    myTable.rows[row].cells[8].innerHTML = '{{ band.memberDefs[troop]['Notes'] }}';

  }
  {% endfor %}
  updateCost();
}
function updateCost()
{
  var sum = 0;
  var myTable = document.getElementById('rosterTable');

  for(i = 1; i <= 8; i++)
  {
    sum = sum + parseInt(myTable.rows[i].cells[7].innerHTML);
  }



  remainingGold.innerHTML = total - sum ;
}

function setCapSkillchoice(x)
{                          
  var choice = x.value;
  var myTable = document.getElementById('wizardTable');
  var list = myTable.rows[1].cells[8];         
  while (list.hasChildNodes()) {
    list.removeChild(list.firstChild);
  }
  if(choice == "Empty")
  {        
    list.appendChild(document.createTextNode("No Skills"));
  }
  {% for spec in band.skillsets.keys() %}
  if(choice == "{{spec}}")
  {
    var div = document.createElement("div");
    {% for element in band.skillsets[spec] %}
    var tempbutton = document.createElement("input");
    tempbutton.setAttribute("type", "checkbox");
    tempbutton.setAttribute("id", "skillchoice");
    tempbutton.setAttribute("name", "skillchoice");
    tempbutton.setAttribute("value", "{{element}}");
    var templabel = document.createElement("label");
    templabel.appendChild(document.createTextNode("{{element}}"));
    div.appendChild(tempbutton);
    div.appendChild(templabel);
    {% endfor %}
    list.appendChild(div);


  }            
  {% endfor %}
}

function setEnsSkillchoice(x)
{
  var choice = x.value;
  var myTable = document.getElementById('ensignTable');
  var list = myTable.rows[1].cells[8];
  while (list.hasChildNodes()) {
    list.removeChild(list.firstChild);
  }
  if(choice == "Empty")
  {
    list.appendChild(document.createTextNode("No Skills"));
  }
  {% for spec in band.skillsets.keys() %}
  if(choice == "{{spec}}")
  {
    var div = document.createElement("div");
    {% for element in band.skillsets[spec] %}
    var tempbutton = document.createElement("input");
    tempbutton.setAttribute("type", "checkbox");
    tempbutton.setAttribute("id", "skillchoiceens");
    tempbutton.setAttribute("name", "skillchoiceens");
    tempbutton.setAttribute("value", "{{element}}");
    var templabel = document.createElement("label");
    templabel.appendChild(document.createTextNode("{{element}}"));
    div.appendChild(tempbutton);
    div.appendChild(templabel);
    {% endfor %}
    list.appendChild(div);


  }
  {% endfor %}
}

function doFormSubmit()
{
  method =  "post"; // Set method to post by default if not specified.

  var form = document.createElement("form");
  form.setAttribute("method", method);
  form.setAttribute("action", "/new");


  /*  bandname = request.form['bandname']
      capspec = request.form['capspec']
      ensspec = request.form['ensspec']
      capskill = request.form['capskill']
      ensskill = request.form['ensskill']
      capweap = request.form['capweap']
      ensweap = request.form['ensweap']
      troops = json.loads(request.form['troops']) */

  var bandname = document.createElement("input");
  bandname.setAttribute("type", "hidden");
  bandname.setAttribute("name", "bandname");
  bandname.setAttribute("value", document.getElementById('bandnametext').value);
  form.appendChild(bandname);
  if( document.getElementById('public').checked ){
    var ispublic = document.createElement("input");
    ispublic.setAttribute("type", "hidden");
    ispublic.setAttribute("name", "ispublic");
    form.appendChild(ispublic);
  }
  var capspec = document.createElement("input");
  capspec.setAttribute("type", "hidden");
  capspec.setAttribute("name", "capspec");
  capspec.setAttribute("value", document.getElementById('capspecsel').value);
  form.appendChild(capspec);
  var skills = [];
  var elem = document.getElementsByName('skillchoice');
  for (var i=0;i<elem.length;i++){
    if ( elem[i].checked ) {
          skills.push(elem[i].value);
    }
  }
  capskill = document.createElement("input");
  capskill.setAttribute("type", "hidden");
  capskill.setAttribute("name", "capskill");
  capskill.setAttribute("value", skills);
  form.appendChild(capskill);
  
  var capweap = document.createElement("input");
  capweap.setAttribute("type", "hidden");
  capweap.setAttribute("name", "capweap");
  capweap.setAttribute("value", document.getElementById('capweapsel').value);
  form.appendChild(capweap);
  if (engHire)
  {
    var hasensign = document.createElement("input");
    hasensign.setAttribute("type", "hidden");
    hasensign.setAttribute("name", "hasensign");
    hasensign.setAttribute("value", engHire);
    form.appendChild(hasensign);
    var ensspec = document.createElement("input");
    ensspec.setAttribute("type", "hidden");
    ensspec.setAttribute("name", "ensspec");
    ensspec.setAttribute("value", document.getElementById('ensskillsel').value);
    form.appendChild(ensspec);
    skills = [];
    var elem = document.getElementsByName('skillchoiceens');
    for (var i=0;i<elem.length;i++){
      if ( elem[i].checked ) {
          skills.push(elem[i].value);
      }
    }
    var ensskill = document.createElement("input");
    ensskill.setAttribute("type", "hidden");
    ensskill.setAttribute("name", "ensskill");
    ensskill.setAttribute("value", skills);
    form.appendChild(ensskill);
    var ensweap = document.createElement("input");
    ensweap.setAttribute("type", "hidden");
    ensweap.setAttribute("name", "ensweap");
    ensweap.setAttribute("value", document.getElementById('ensweapsel').value);
    form.appendChild(ensweap);
  }

  var troopsarr = []
    for(i = 1; i < 11; i++)
    {       
      var selector = document.getElementById('troop'+i);
      troopsarr.push(selector.value);
    }
  var troops = document.createElement("input");
  troops.setAttribute("type", "hidden");
  troops.setAttribute("name", "troops");
  troops.setAttribute("value", JSON.stringify(troopsarr));
  form.appendChild(troops);


  document.body.appendChild(form);
  form.submit();
}

