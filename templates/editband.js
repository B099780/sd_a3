var total = {{ band.money }};
{% if band.hasEnsign == True  %}
var engHire = true;
var hadEnsign = true;
{%else%}
var engHire = false;
var hadEnsign = false;
{% endif %} 
function hireEng()
{ 
  if(engHire == false)
  {
    engHire = true;
    document.getElementById("ensignDiv").style.display = "block";
    total = total - 200;
    updateCost();
  }
  else
  {
    engHire = false;
    document.getElementById("ensignDiv").style.display = "none";
    total = total + 200;
    updateCost();
  }
}

function updateTableRow(row, x)
{
  var sel = x.value;

  var myTable = document.getElementById('rosterTable');

  if(sel == "Empty")
  {
    myTable.rows[row].cells[1].innerHTML = '-';
    myTable.rows[row].cells[2].innerHTML = '-';
    myTable.rows[row].cells[3].innerHTML = '-';
    myTable.rows[row].cells[4].innerHTML = '-';
    myTable.rows[row].cells[5].innerHTML = '-'
    myTable.rows[row].cells[6].innerHTML = '-';
    myTable.rows[row].cells[7].innerHTML = 0;
    myTable.rows[row].cells[8].innerHTML = '-';
  }
  {% for troop in band.memberDefs.keys() %}
  if(sel == "{{troop}}")
  {
    myTable.rows[row].cells[1].innerHTML = '{{ band.memberDefs[troop]['Move'] }}';
    myTable.rows[row].cells[2].innerHTML = '{{ band.memberDefs[troop]['Fight'] }}';
    myTable.rows[row].cells[3].innerHTML = '{{ band.memberDefs[troop]['Shoot'] }}';
    myTable.rows[row].cells[4].innerHTML = '{{ band.memberDefs[troop]['Shield'] }}';
    myTable.rows[row].cells[5].innerHTML = '{{ band.memberDefs[troop]['Morale'] }}';
    myTable.rows[row].cells[6].innerHTML = '{{ band.memberDefs[troop]['Health'] }}';
    myTable.rows[row].cells[7].innerHTML = {{ band.memberDefs[troop]['Cost'] }};
    myTable.rows[row].cells[8].innerHTML = '{{ band.memberDefs[troop]['Notes'] }}';

  }
  {% endfor %}
  updateCost();
}
function updateCost()
{
  var sum = 0;
  var myTable = document.getElementById('rosterTable');

  for(i = 1; i <= 8; i++)
  {
    sum = sum + parseInt(myTable.rows[i].cells[7].innerHTML);
  }



  remainingGold.innerHTML = total - sum ;
}

function setCapSkillchoice(x)
{                          
  var choice = x.value;
  var myTable = document.getElementById('wizardTable');
  var list = myTable.rows[1].cells[8];         
  while (list.hasChildNodes()) {
    list.removeChild(list.firstChild);
  }
  if(choice == "Empty")
  {        
    list.appendChild(document.createTextNode("No Skills"));
  }
  {% for spec in band.skillsets.keys() %}
  if(choice == "{{spec}}")
  {
    var div = document.createElement("div");
    {% for element in band.skillsets[spec] %}
    var tempbutton = document.createElement("input");
    tempbutton.setAttribute("type", "checkbox");
    tempbutton.setAttribute("id", "skillchoice");
    tempbutton.setAttribute("name", "skillchoice");
    tempbutton.setAttribute("value", "{{element}}");
    var templabel = document.createElement("label");
    templabel.appendChild(document.createTextNode("{{element}}"));
    div.appendChild(tempbutton);
    div.appendChild(templabel);
    {% endfor %}
    list.appendChild(div);
  }            
  {% endfor %}
}

function setEnsSkillchoice(x)
{
  var choice = x.value;
  var myTable = document.getElementById('ensignTable');
  var list = myTable.rows[1].cells[8];
  while (list.hasChildNodes()) {
    list.removeChild(list.firstChild);
  }
  if(choice == "Empty")
  {
    list.appendChild(document.createTextNode("No Skills"));
  }
  {% for spec in band.skillsets.keys() %}
  if(choice == "{{spec}}")
  {
    var div = document.createElement("div");
    {% for element in band.skillsets[spec] %}
    var tempbutton = document.createElement("input");
    tempbutton.setAttribute("type", "checkbox");
    tempbutton.setAttribute("id", "skillchoiceens");
    tempbutton.setAttribute("name", "skillchoiceens");
    tempbutton.setAttribute("value", "{{element}}");
    var templabel = document.createElement("label");
    templabel.appendChild(document.createTextNode("{{element}}"));
    div.appendChild(tempbutton);
    div.appendChild(templabel);
    {% endfor %}
    list.appendChild(div);


  }
  {% endfor %}
}

function doFormSubmit()
{ 
  method =  "post"; // Set method to post by default if not specified.
  
  var form = document.createElement("form");
  form.setAttribute("method", method);
  form.setAttribute("action", "/edit/{{ band.name }}");

  var ispublic = document.createElement("input");
  ispublic.setAttribute("type", "hidden");
  ispublic.setAttribute("name", "ispublic");
  if(document.getElementById('public').checked){ispublic.setAttribute("value", true);}
  else{ ispublic.setAttribute("value", false); } 
  form.appendChild(ispublic);
  var capspec = document.createElement("input");
  capspec.setAttribute("type", "hidden");
  capspec.setAttribute("name", "capspec");
  capspec.setAttribute("value", document.getElementById('capspecsel').value);
  form.appendChild(capspec);
  //start
  var capmove = document.createElement("input");
  capmove.setAttribute("type", "hidden");
  capmove.setAttribute("name", "capmove");
  capmove.setAttribute("value", document.getElementById('capmov').value);
  form.appendChild(capmove);
  var capfight = document.createElement("input");
  capfight.setAttribute("type", "hidden");
  capfight.setAttribute("name", "capfight");
  capfight.setAttribute("value", document.getElementById('capfig').value);
  form.appendChild(capfight);
  var capshoot = document.createElement("input");
  capshoot.setAttribute("type", "hidden");
  capshoot.setAttribute("name", "capshoot");
  capshoot.setAttribute("value", document.getElementById('capsho').value);
  form.appendChild(capshoot);
  var capshield = document.createElement("input");
  capshield.setAttribute("type", "hidden");
  capshield.setAttribute("name", "capshield");
  capshield.setAttribute("value", document.getElementById('capshi').value);
  form.appendChild(capshield);
  var capmorale = document.createElement("input");
  capmorale.setAttribute("type", "hidden");
  capmorale.setAttribute("name", "capmorale");
  capmorale.setAttribute("value", document.getElementById('capmor').value);
  form.appendChild(capmorale);
  var caphealth = document.createElement("input");
  caphealth.setAttribute("type", "hidden");
  caphealth.setAttribute("name", "caphealth");
  caphealth.setAttribute("value", document.getElementById('caphea').value);
  form.appendChild(caphealth);
  var capexperience = document.createElement("input");
  capexperience.setAttribute("type", "hidden");
  capexperience.setAttribute("name", "capexperience");
  capexperience.setAttribute("value", document.getElementById('capexp').value);
  form.appendChild(capexperience);     
  //end
  var capskills = []
    var capskillarr =  document.getElementsByName("capskil");
  for(k=0;k<capskillarr.length;k++)
  {
    if (capskillarr[k].checked)
    {
      capskills.push(capskillarr[k].value);
    }
  }
  var capskill = document.createElement("input");
  capskill.setAttribute("type", "hidden");
  capskill.setAttribute("name", "capskill");
  capskill.setAttribute("value", capskills);
  form.appendChild(capskill);
  var capweap = document.createElement("input");
  capweap.setAttribute("type", "hidden");
  capweap.setAttribute("name", "capweap");
  capweap.setAttribute("value", document.getElementById('capweapsel').value);
  form.appendChild(capweap);

  var hasensign = document.createElement("input");
  hasensign.setAttribute("type", "hidden");
  hasensign.setAttribute("name", "hasensign");
  hasensign.setAttribute("value", engHire);
  form.appendChild(hasensign);
  if (engHire)
  {
    var ensspec = document.createElement("input");
    ensspec.setAttribute("type", "hidden");
    ensspec.setAttribute("name", "ensspec");
    ensspec.setAttribute("value", document.getElementById('ensskillsel').value);
    form.appendChild(ensspec);

    //start
    var enmove = document.createElement("input");
    enmove.setAttribute("type", "hidden");
    enmove.setAttribute("name", "ensmove");
    enmove.setAttribute("value", document.getElementById('enmov').value);
    form.appendChild(enmove);
    var enfight = document.createElement("input");
    enfight.setAttribute("type", "hidden");
    enfight.setAttribute("name", "ensfight");
    enfight.setAttribute("value", document.getElementById('enfig').value);
    form.appendChild(enfight);
    var enshoot = document.createElement("input");
    enshoot.setAttribute("type", "hidden");
    enshoot.setAttribute("name", "ensshoot");
    enshoot.setAttribute("value", document.getElementById('ensho').value);
    form.appendChild(enshoot);
    
    var enshield = document.createElement("input");
    enshield.setAttribute("type", "hidden");
    enshield.setAttribute("name", "ensshield");
    enshield.setAttribute("value", document.getElementById('enshi').value);
    form.appendChild(enshield);
    var enmorale = document.createElement("input");
    enmorale.setAttribute("type", "hidden");
    enmorale.setAttribute("name", "ensmorale");
    enmorale.setAttribute("value", document.getElementById('enmor').value);
    form.appendChild(enmorale);
    var enhealth = document.createElement("input");
    enhealth.setAttribute("type", "hidden");
    enhealth.setAttribute("name", "enshealth");
    enhealth.setAttribute("value", document.getElementById('enhea').value);
    form.appendChild(enhealth);
    var enexperience = document.createElement("input");
    enexperience.setAttribute("type", "hidden");
    enexperience.setAttribute("name", "ensexperience");
    enexperience.setAttribute("value", document.getElementById('enexp').value);
    form.appendChild(enexperience);
    //end

      var ensskills = [];
        var ensskillarr =  document.getElementsByName("ensskil");
      for(k=0;k<ensskillarr.length;k++)
      {
        if (ensskillarr[k].checked)
        {
          ensskills.push(ensskillarr[k].value);
        }
      }

      var ensskill = document.createElement("input");
      ensskill.setAttribute("type", "hidden");
      ensskill.setAttribute("name", "ensskill");
      ensskill.setAttribute("value", ensskills);
      form.appendChild(ensskill);
    var ensweap = document.createElement("input");
    ensweap.setAttribute("type", "hidden");
    ensweap.setAttribute("name", "ensweap");
    ensweap.setAttribute("value", document.getElementById('ensweapsel').value);
    form.appendChild(ensweap);

  }

  var troopsarr = []
    for(i = 1; i < 11; i++)
    {       
      var selector = document.getElementById('troop'+i);
      troopsarr.push(selector.value);
    }
  var troops = document.createElement("input");
  troops.setAttribute("type", "hidden");
  troops.setAttribute("name", "troops");
  troops.setAttribute("value", JSON.stringify(troopsarr));
  form.appendChild(troops);


  document.body.appendChild(form);
  form.submit();
}
